mininet (2.2.2-5) unstable; urgency=medium

  [ Santiago Ruano Rincón ]
  * debian/control: replace Depends: cgroup-bin with cgroup-tools
    (Closes: #926279)

 -- Tomasz Buchert <tomasz@debian.org>  Mon, 15 Apr 2019 00:28:46 +0200

mininet (2.2.2-4) unstable; urgency=medium

  * refresh packaging (debhelper 12, std-ver 4.3.0)

 -- Tomasz Buchert <tomasz@debian.org>  Thu, 10 Jan 2019 01:06:57 +0100

mininet (2.2.2-3) unstable; urgency=medium

  * d/control: update vcs links to salsa
  * d/tests: depend on openvswitch-switch (Closes: #889700)
  * d/*: update compat to 11
  * Fix insecure-copyright-format-uri

 -- Tomasz Buchert <tomasz@debian.org>  Wed, 07 Feb 2018 18:54:03 +0100

mininet (2.2.2-2) unstable; urgency=medium

  [ Santiago R.R.]
  * Fixed detection of ovs-testcontroller path (Closes: #881328)

  [ Tomasz Buchert ]
  * Make openvswitch-switch a recommended dependency (Closes: #882205)

 -- Tomasz Buchert <tomasz@debian.org>  Sun, 03 Dec 2017 14:29:47 +0100

mininet (2.2.2-1) unstable; urgency=medium

  * New upstream version 2.2.2

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 25 Mar 2017 12:21:31 +0100

mininet (2.2.1-4) unstable; urgency=medium

  * d/control: enable CI tests for real

 -- Tomasz Buchert <tomasz@debian.org>  Sun, 01 Jan 2017 20:51:07 +0100

mininet (2.2.1-3) unstable; urgency=medium

  * d/control: bumped std-ver to 3.9.8 (no changes needed)
  * d/control: use debhelper 10
  * d/watch: use watch version 4
  * d/tests: added CI tests
  * d/control: add missing dependency (net-tools)

 -- Tomasz Buchert <tomasz@debian.org>  Sun, 01 Jan 2017 15:19:09 +0100

mininet (2.2.1-2) unstable; urgency=medium

  * Make container mounts private (Closes: #799654)
  * Switch the build process to pybuild

 -- Tomasz Buchert <tomasz@debian.org>  Fri, 06 Nov 2015 11:39:22 +0100

mininet (2.2.1-1) unstable; urgency=medium

  * d/control: Fix VCS link
  * d/control: Limit to Linux architectures
  * Imported Upstream version 2.2.1

 -- Tomasz Buchert <tomasz@debian.org>  Thu, 18 Jun 2015 09:42:11 +0200

mininet (2.2.0-1) unstable; urgency=medium

  * Initial release (Closes: #775192)

 -- Tomasz Buchert <tomasz@debian.org>  Mon, 12 Jan 2015 11:45:31 +0100
